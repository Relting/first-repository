#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;
	
template <typename T>
class List
{
private:
	// private member variables
	int m_itemCount = 0;
	T m_arr[ARRAY_SIZE];

	// functions for internal-workings
	bool ShiftRight(int atIndex)
	{

		if (m_itemCount <= atIndex)
			return false;

		for (int i = m_itemCount; i > atIndex; i--)
		{
			m_arr[i] = m_arr[i - 1];
		}
		return true; // CHECK. DO I ADD TO ITEMCOUNT?
	}

	bool ShiftLeft(int atIndex)
	{
		if (m_itemCount < 2 || m_itemCount <= (atIndex + 1))
		{
			return false;
		}

		for (int i = atIndex; i < (m_itemCount - 1); i++)
		{
			m_arr[i] = m_arr[i + 1];
		}
		return true;
	}

public:
	List()
	{
	}

	~List()
	{
	}

	// Core functionality
	int     Size() const      // CHECK
	{
		return m_itemCount;
	}

	bool    IsEmpty() const
	{
		return (m_itemCount == 0);
	}

	bool    IsFull() const
	{
		return (m_itemCount == ARRAY_SIZE);
	}

	bool    PushFront(const T& newItem)
	{
		return Insert(0, newItem);
	}

	bool    PushBack(const T& newItem)
	{
		//return Insert(m_itemCount, newItem);
		if (m_itemCount < ARRAY_SIZE)
		{
			m_arr[m_itemCount] = newItem;
			m_itemCount++;
			return true;
		}
		else
			return false;

	}

	bool    Insert(int atIndex, const T& item)
	{
		if (m_itemCount < ARRAY_SIZE && m_itemCount >= atIndex)
		{
			ShiftRight(atIndex);
			m_itemCount++;
			m_arr[atIndex] = item;
			return true;
		}
		else
			return false;

	}

	bool    PopFront()
	{
		if(m_itemCount == 0)
		{
			return false;
		}

		ShiftLeft(0);
		m_itemCount--;
		return true;
	}
	bool    PopBack()
	{
		if (m_itemCount == 0)
			return false;
		else
		{
			m_itemCount--;
			return true;
		}

		return false;
	}

	bool    RemoveItem(const T& item)
	{
		bool DoesItContain = Contains(item);

		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
			{
				ShiftLeft(i);
				m_itemCount--;
			}
		}
		return DoesItContain;
	}

	bool    RemoveIndex(int atIndex)
	{
		if(atIndex >= m_itemCount)
		{
			return false;
		}

		ShiftLeft(atIndex);    
		m_itemCount--;
		return true;
	}

	void    Clear()
	{
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)
	{
		if (m_itemCount <= atIndex)
			return nullptr;
		else
			return &m_arr[atIndex];
	}

	T*      GetFront()
	{
		if (m_itemCount == 0)
			return nullptr;
		else
			return &m_arr[0]; // CHEEEEECK
	}

	T*      GetBack()
	{
		if (m_itemCount == 0)
			return nullptr;

		return &m_arr[(m_itemCount - 1)];
	}

	// Additional functionality
	int     GetCountOf(const T& item) const
	{
		int counter = 0;
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
				counter++;
		}

		return counter;
	}

	bool    Contains(const T& item) const
	{
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
				return true;
		}
		return false;
	}

	friend class Tester;
};

#endif
