#include <iostream>
#include <string>
#include "Tester.hpp"

using namespace std;

int main()
{
    Tester tester;
    tester.RunTests();
	cout << "Got through all the tests!" << endl;
	cin.ignore();
	cin.get();
	return 0;
}


