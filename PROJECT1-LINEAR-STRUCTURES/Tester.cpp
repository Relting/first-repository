#include "Tester.hpp"

void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_RemoveItem();
	Test_RemoveIndex();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}


void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;
	 // Do not need to test. Could test if size was zero, but no need.
    // Put tests here
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

	{   // Test begin do nothing
		cout << endl << "Test 1" << endl;
		List<int> testList;

		bool expected = false;
		bool actual = testList.ShiftRight(1);

		cout << "Expected shift: " << expected << endl;
		cout << "Actual shift:   " << actual<< endl;

		if (actual == expected)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin; push A B C
		cout << endl << "Test 2" << endl;  
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expected = true;
		bool actual = testList.ShiftRight(1);
		//if (testList.Get(1) == 0)
		//{
		//	cout << " Get(1) is a nullptr!" << endl;
		//}
		//else
			/*((testList.Get(1) != = 0) ? (testList.Get(1)) : "Get was nullptr!")*/  // THIS IS NOT WORKING, HOW DO I TEST IF NULLPTR

		cout << "Expected shift: " << expected << endl;
		cout << "Actual shift:   " << actual << endl;
		cout << "Expected character at position 1: " << "B" << endl;
		cout << "Actual character at position 1  : " << *testList.Get(1) << endl; // will this do what I want it to do?
		cout << "Expected character at position 2: " << "B" << endl;
		cout << "Actual character at position 2  : " << *testList.Get(2) << endl;

		if (actual == expected && 'B' == *(testList.Get(1)) && 'B' == *testList.Get(2))
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

	{   // Test begin do nothing
		cout << endl << "Test 1" << endl;
		List<int> testList;

		bool expected = false;
		bool actual = testList.ShiftLeft(1);

		cout << "Expected shift: " << expected << endl;
		cout << "Actual shift:   " << actual << endl;

		if (actual == expected)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin; push A B C
		cout << endl << "Test 2" << endl;
		List<int> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expected = true;
		bool actual = testList.ShiftLeft(1);

		cout << "Expected shift: " << expected << endl;
		cout << "Actual shift:   " << actual << endl;
		cout << "Expected character at position 1: " << "C" << endl;
		cout << "Actual character at position 1  : " << char(*(testList.Get(1))) << endl;  //It was outputting the ASCII char for C!

		if (actual == expected && 'C' == *(testList.Get(1))) // Check if it's nullptr first? How to compare?
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin do nothing
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

    {   // Test begin push 1
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack( 1 );

        int expectedSize = 1;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

	{   // Test begin push 5 pop 3
		cout << endl << "Test 3" << endl;
		List<int> testList;


		for (int i = 0; i < 5; i++)
		{
			testList.PushBack(1);
		}

		for (int i = 0; i < 3; i++)
		{
			testList.PopBack();
		}

		int expectedSize = 2;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;


	{   // Test begin do nothing
		cout << endl << "Test 1" << endl;
		List<int> testList;
		bool expectedEmptiness = true;
		bool actualEmptiness = testList.IsEmpty();

		cout << "Expected emptiness: " << expectedEmptiness << endl;
		cout << "Actual emptiness:   " << actualEmptiness << endl;

		if (actualEmptiness == expectedEmptiness)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin push 101
		cout << endl << "Test 2" << endl;
		List<int> testList;

		for (int i = 0; i < 101; i++)
		{
			testList.PushBack(1);
		}

		bool expectedEmptiness = false;
		bool actualEmptiness = testList.IsEmpty();

		cout << "Expected emptiness: " << expectedEmptiness << endl;
		cout << "Actual emptiness:   " << actualEmptiness << endl;

		if (actualEmptiness == expectedEmptiness)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin     push 5 pop 5
		cout << endl << "Test 3" << endl;
		List<int> testList;

		for (int i = 0; i < 5; i++)
		{
			testList.PushBack(1);
		}

		for (int i = 0; i < 5; i++)
		{
			testList.PopBack();
		}

		bool expectedEmptiness = true;
		bool actualEmptiness = testList.IsEmpty();

		cout << "Expected emptiness: " << expectedEmptiness << endl;
		cout << "Actual emptiness:   " << actualEmptiness << endl;

		if (actualEmptiness == expectedEmptiness)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end



}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

	{   // Test begin do nothing
		cout << endl << "Test 1" << endl;
		List<int> testList;
		bool expectedFullness = false;
		bool actualFullness = testList.IsFull();

		cout << "Expected Fullness: " << expectedFullness << endl;
		cout << "Actual Fullness:   " << actualFullness << endl;

		if (actualFullness == expectedFullness)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin insert 100 items
		cout << endl << "Test 2" << endl;
		List<int> testList;

		for (int i = 0; i < 100; i++)
		{
			testList.PushBack(1);
		}

		bool expectedFullness = true;
		bool actualFullness = testList.IsFull();

		cout << "Expected Fullness: " << expectedFullness << endl;
		cout << "Actual Fullness:   " << actualFullness << endl;

		if (actualFullness == expectedFullness)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin insert 101 items
		cout << endl << "Test 3" << endl;
		List<int> testList;

		for (int i = 0; i < 101; i++)
		{
			testList.PushBack(1);
		}

		bool expectedFullness = true;
		bool actualFullness = testList.IsFull();

		cout << "Expected Fullness: " << expectedFullness << endl;
		cout << "Actual Fullness:   " << actualFullness << endl;

		if (actualFullness == expectedFullness)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

	{   // Test begin Create a List, PushBack �A� �B� �C� (Will be at 0, 1, 2)
		cout << endl << "Test 1" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');
		
		bool expectedPush = true;
		bool actualPush = testList.PushFront('D');

		cout << "Expected push: " << expectedPush << endl;
		cout << "Actual push:   " << actualPush << endl;
		cout << "Expected size: " << "4" << endl;
		cout << "Actual size:   " << testList.Size() << endl;
		cout << "Expected character at position 0: " << "D" << endl;
		cout << "Actual character at position 0: " << *(testList.Get(0)) << endl;
		cout << "Expected character at position 1: " << "A" << endl;
		cout << "Actual character at position 1: " << *(testList.Get(1)) << endl;

		if (actualPush == expectedPush && testList.Size() == 4 && *(testList.Get(0)) == 'D' && *(testList.Get(1)) == 'A')
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin Create a List of 100 �A�s
		cout << endl << "Test 2" << endl;
		List<char> testList;

		for (int i = 0; i < 100; i++)
		{
			testList.PushBack('A');
		}

		bool expectedPush = false;
		bool actualPush = testList.PushFront('D');

		cout << "Expected push: " << expectedPush << endl;
		cout << "Actual push:   " << actualPush << endl;
		cout << "Expected size: " << "100" << endl;
		cout << "Actual size:   " << testList.Size() << endl;
		cout << "Expected character at position 0: " << "A" << endl;
		cout << "Actual character at position 0: " <<*(testList.Get(0)) << endl;

		if (actualPush == expectedPush && *(testList.Get(0)) == 'A' && testList.Size() == 100)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

	{   // Test begin add one to empty list
		cout << endl << "Test 1" << endl;
		List<char> testList;
		bool expectedPushBack = true;
		bool actualPushBack = testList.PushBack('A');

		cout << "Expected PushBack: " << expectedPushBack << endl;
		cout << "Actual PushBack:   " << actualPushBack << endl;
		cout << "Expected size:     " << 1 << endl;
		cout << "Actual size:       " << testList.Size() << endl;
		if (actualPushBack == expectedPushBack && testList.Size() == 1)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin insert 90 items
		cout << endl << "Test 2" << endl;
		List<char> testList;

		for (int i = 0; i < 90; i++)
		{
			testList.PushBack(8);
		}

		bool expectedPushBack = true;
		bool actualPushBack = testList.PushBack('B');

		cout << "Expected PushBack: " << expectedPushBack << endl;
		cout << "Actual PushBack:   " << actualPushBack << endl;
		cout << "Expected size:     " << 91 << endl;
		cout << "Actual size:       " << testList.Size() << endl;
		cout << "Expected character at position 90: " << "B" << endl;
		cout << "Actual character at position 90: " << *(testList.Get(90)) << endl;    //CHECK IF IT IS NULL FIRST

		if (actualPushBack == expectedPushBack && testList.Size() == 91 && *(testList.Get(90)) == 'B')
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin insert 100 items
		cout << endl << "Test 3" << endl;
		List<char> testList;

		for (int i = 0; i < 100; i++)
		{
			testList.PushBack(1);
		}

		bool expectedPushBack = false;
		bool actualPushBack = testList.PushBack('B');

		cout << "Expected PushBack: " << expectedPushBack << endl;
		cout << "Actual PushBack:   " << actualPushBack << endl;
		cout << "Expected size:     " << 100 << endl;
		cout << "Actual size:       " << testList.Size() << endl;

		if (actualPushBack == expectedPushBack && testList.Size() == 100)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

	{   // Test begin ; create a list, do nothing
		cout << endl << "Test 1" << endl;
		List<char> testList;

		bool expectedPop = false;
		bool actualPop = testList.PopFront();

		cout << "Expected PopFront: " << expectedPop << endl;
		cout << "Actual PopFront:   " << actualPop << endl;

		if (actualPop == expectedPop)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin insert A B C
		cout << endl << "Test 2" << endl;
		List<char> testList;

			testList.PushBack('A');
			testList.PushBack('B');
			testList.PushBack('C');

		bool expectedPop = true;
		bool actualPop = testList.PopFront();

		char expectedValueAt0 ='B';
		char* actualValueAt0 = testList.Get(0);

		if (actualValueAt0 == nullptr) {
			cout << "Nullptr" << endl;
			return;
		}

		cout << "Expected Popfront: " << expectedPop << endl;
		cout << "Actual Popfront  : " << actualPop << endl;
		cout << "Expected size:     " << 2 << endl;
		cout << "Actual size  :     " << testList.Size() << endl;
		cout << "Expected character at position 0: " << expectedValueAt0 << endl;
		cout << "Actual character at position 0: " << *(actualValueAt0) << endl;

		if (actualPop != expectedPop)
		{
			cout << "Fail - bad pop return" << endl;
		}
		else if (testList.Size() != 2)
		{
			cout << "Fail - bad size" << endl;
		}
		else if (*actualValueAt0 != expectedValueAt0)
		{
			cout << "Fail - bad value" << endl;
		}
		else
		{
			cout << "Pass" << endl;
		}
		

	//	if (actualPop == expectedPop && testList.Size() == 2 && 'B' == *(testList.Get(0)))
	//	{
	//		cout << "Pass" << endl;
	//	}
	//	else
	//	{
	//		cout << "Fail" << endl;
	//	}
	}   // Test end                 IF I HAVE TIME, FIX OTHER TESTS TO WORK LIKE THIS ???????????????
  
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

	{   // Test begin ; create a list, do nothing
		cout << endl << "Test 1" << endl;
		List<char> testList;

		bool expectedPop = false;
		bool actualPop = testList.PopBack();

		cout << "Expected PopBack: " << expectedPop << endl;
		cout << "Actual PopBack:   " << actualPop << endl;

		if (actualPop == expectedPop)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin insert A B C
		cout << endl << "Test 2" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expectedPop = true;
		bool actualPop = testList.PopBack();

		cout << "Expected PopBack: " << expectedPop << endl;
		cout << "Actual PopBack  : " << actualPop << endl;
		cout << "Expected size:     " << 2 << endl;
		cout << "Actual size:       " << testList.Size() << endl;

		//Originally wanted to test if position 2 returned nullptr. Don't worry anymore, it's lazy deletion!
		if (actualPop == expectedPop && testList.Size() == 2)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;


	{   // Test begin; do nothing
		cout << endl << "Test 1" << endl;
		List<char> testList;
		
		testList.Clear();

		cout << "Expected size:     " << 0 << endl;
		cout << "Actual size:       " << testList.Size() << endl;

		if (testList.Size() == 0)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin; insert A B C
		cout << endl << "Test 2" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		testList.Clear();


		cout << "Expected size:     " << 0 << endl;
		cout << "Actual size:       " << testList.Size() << endl;

		if (testList.Size() == 0)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

	{   // Test begin Create a List, PushBack �A� �B� �C� (Will be at 0, 1, 2)
		cout << endl << "Test 1" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		char expectedGet = 'B';
		char actualGet = *(testList.Get(1));

		cout << "Expected get: " << expectedGet << endl;
		cout << "Actual get:   " << actualGet << endl;

		if (actualGet == expectedGet)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin Create a List, do nothing
		cout << endl << "Test 2" << endl;
		List<char> testList;
		
		char *expectedGet = nullptr;
		char *actualGet = testList.Get(1);

		cout << "Expected address: " << (int)expectedGet << endl;  // Typecast null ptr to int 0
		cout << "Actual address:   " << (int)actualGet << endl;

		if (actualGet == expectedGet)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

	{   // Test begin Create a List, do nothing
		cout << endl << "Test 1" << endl;
		List<char> testList;

		char *expectedGet = nullptr;
		char *actualGet = testList.GetFront();      

		cout << "Expected get: " << (int)expectedGet << endl;
		cout << "Actual get:   " << (int)actualGet << endl;

		if (actualGet == expectedGet)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin Create a List, PushBack �A� �B� �C� (Will be at 0, 1, 2)
		cout << endl << "Test 2" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		char expectedGet = 'A';
		char actualGet = *testList.GetFront();

		cout << "Expected get: " << expectedGet << endl;
		cout << "Actual get:   " << actualGet << endl;

		if (actualGet == expectedGet)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

	{   // Test begin Create a List, do nothing    
		cout << endl << "Test 1" << endl;
		List<char> testList;

		char* expectedGet = nullptr;
		char* actualGet = testList.GetBack();

		cout << "Expected get: " << (int)expectedGet << endl;
		cout << "Actual get:   " << (int)actualGet << endl;

		if (actualGet == expectedGet)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin Create a List, PushBack �A� �B� �C� (Will be at 0, 1, 2)
		cout << endl << "Test 2" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');

		char expectedGet = 'B';
		char actualGet = *testList.GetBack();

		cout << "Expected get: " << expectedGet << endl;
		cout << "Actual get:   " << actualGet << endl;

		if (actualGet == expectedGet)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

	{   // Test begin do nothing
		cout << endl << "Test 1" << endl;
		List<char> testList;
		int expectedCount = 0;
		int actualCount = testList.GetCountOf('c');

		cout << "Expected count: " << expectedCount << endl;
		cout << "Actual count:   " << actualCount << endl;

		if (actualCount == expectedCount)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin Create a List, add 5 items not including the item to be searched
		cout << endl << "Test 2" << endl;
		List<char> testList;

		for (int i = 0; i < 5; i++)
		{
			testList.PushBack(1);
		}

		int expectedCount = 0;
		int actualCount = testList.GetCountOf('c');

		cout << "Expected count: " << expectedCount << endl;
		cout << "Actual count:   " << actualCount << endl;

		if (actualCount == expectedCount)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end



	{   // Test begin Create a list, insert 5 items not c, insert 5 �c��s
		cout << endl << "Test 3" << endl;
		List<char> testList;

		for (int i = 0; i < 5; i++)
		{
			testList.PushBack(1);
		}

		for (int i = 0; i < 5; i++)
		{
			testList.PushBack('c');
		}

		int expectedCount = 5;
		int actualCount = testList.GetCountOf('c');

		cout << "Expected count: " << expectedCount << endl;
		cout << "Actual count:   " << actualCount << endl;
		
		if (actualCount == expectedCount)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end



	{   // Test begin Create a list, insert 105 �A�s
		cout << endl << "Test 4" << endl;
		List<char> testList;

		for (int i = 0; i < 105; i++)
		{
			testList.PushBack('A');
		}


		int expectedCount = 100;
		int actualCount = testList.GetCountOf('A');

		cout << "Expected count: " << expectedCount << endl;
		cout << "Actual count:   " << actualCount << endl;

		if (actualCount == expectedCount)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end



}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

	{   // Test begin do nothing
		cout << endl << "Test 1" << endl;
		List<char> testList;
		bool expectedContains = false;
		bool actualContains = testList.Contains('c');

		cout << "Expected contains: " << expectedContains << endl;
		cout << "Actual contains:   " << actualContains << endl;

		if (actualContains == expectedContains)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end



	{   // Test begin Create a List, add 5 items not including the item to be searched
		cout << endl << "Test 2" << endl;
		List<char> testList;

		for (int i = 0; i < 5; i++)
		{
			testList.PushBack(1);
		}

		bool expectedContains = false;
		bool actualContains = testList.Contains('c');

		cout << "Expected contains: " << expectedContains << endl;
		cout << "Actual contains:   " << actualContains << endl;

		if (actualContains == expectedContains)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin Create a list, insert 5 items not c, insert 5 �c��s
		cout << endl << "Test 3" << endl;
		List<char> testList;

		for (int i = 0; i < 5; i++)
		{
			testList.PushBack(1);
		}

		for (int i = 0; i < 5; i++)
		{
			testList.PushBack('c');
		}

		bool expectedContains = true;
		bool actualContains = testList.Contains('c');

		cout << "Expected contains: " << expectedContains << endl;
		cout << "Actual contains:   " << actualContains << endl;

		if (actualContains == expectedContains)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin Create a list, insert 105 "A"s
		cout << endl << "Test 4" << endl;
		List<char> testList;

		for (int i = 0; i < 105; i++)
		{
			testList.PushBack('A');
		}

		bool expectedContains = true;
		bool actualContains = testList.Contains('A');

		cout << "Expected contains: " << expectedContains << endl;
		cout << "Actual contains:   " << actualContains << endl;

		if (actualContains == expectedContains)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


}

void Tester::Test_RemoveItem()
{
    DrawLine();
    cout << "TEST: Test_RemoveItem" << endl;

	{   // Test begin ; create list, do nothing
		cout << endl << "Test 1" << endl;
		List<char> testList;

		bool expected = false;
		bool actual = testList.RemoveItem('B');

		cout << "Expected Remove: " << expected << endl;
		cout << "Actual Remove  : " << actual << endl;

		if (actual == expected)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin insert A B C
		cout << endl << "Test 2" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expected = true;
		bool actual = testList.RemoveItem('B');

		cout << "Expected Remove: " << expected << endl;
		cout << "Actual Remove  : " << actual << endl;
		cout << "Expected character at position 1: " << 'C' << endl;
		cout << "Actual character at position 1  : " << *(testList.Get(1)) << endl; 

		if (actual == expected && 'C' == *(testList.Get(1)))
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin insert A B C
		cout << endl << "Test 3" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expected = false;
		bool actual = testList.RemoveItem('G');

		cout << "Expected Remove: " << expected << endl;
		cout << "Actual Remove  : " << actual << endl;

		if (actual == expected)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

}


void Tester::Test_RemoveIndex()
{
	DrawLine();
	cout << "TEST: Test_RemoveIndex" << endl;


	{   // Test begin ; create list, do nothing
		cout << endl << "Test 1" << endl;
		List<char> testList;

		bool expected = false;
		bool actual = testList.RemoveIndex('2');

		cout << "Expected Remove: " << expected << endl;
		cout << "Actual Remove  : " << actual << endl;

		if (actual == expected)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin insert A B C
		cout << endl << "Test 2" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expected = true;
		bool actual = testList.RemoveIndex(0);

		cout << "Expected Remove: " << expected << endl;
		cout << "Actual Remove  : " << actual << endl;
		cout << "Expected character at position 0: " << "B" << endl;
		cout << "Actual character at position 0  : " <<  *(testList.Get(0)) << endl;

		if (actual == expected && 'B' == *(testList.Get(0))) 
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin insert A B C
		cout << endl << "Test 3" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expected = false;
		bool actual = testList.RemoveIndex(5);

		cout << "Expected Remove: " << expected << endl;
		cout << "Actual Remove  : " << actual << endl;

		if (actual == expected)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;

	{   // Test begin Create a List, PushBack �A� �B� �C� (Will be at 0, 1, 2)
		cout << endl << "Test 1" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expectedInsert = false;
		bool actualInsert = testList.Insert(5, 'D');

		cout << "Expected push: " << expectedInsert << endl;
		cout << "Actual push:   " << actualInsert << endl;
		cout << "Expected size: " << "3" << endl;
		cout << "Actual size:   " << testList.Size() << endl;

		if (actualInsert == expectedInsert && testList.Size() == 3 )
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


	{   // Test begin Create a List, PushBack �A� �B� �C� (Will be at 0, 1, 2)
		cout << endl << "Test 2" << endl;
		List<char> testList;

		testList.PushBack('A');
		testList.PushBack('B');
		testList.PushBack('C');

		bool expectedInsert = true;
		bool actualInsert = testList.Insert(1, 'D');

		cout << "Expected push: " << expectedInsert << endl;
		cout << "Actual push:   " << actualInsert << endl;
		cout << "Expected size: " << "4" << endl;
		cout << "Actual size:   " << testList.Size() << endl;
		cout << "Expected character at position 1: " << "D" << endl;
		cout << "Actual character at position 1: " << *(testList.Get(1)) << endl;
		cout << "Expected character at position 2: " << "B" << endl;
		cout << "Actual character at position 2: " << *(testList.Get(2))<< endl;

		if (actualInsert == expectedInsert && testList.Size() == 4 && *(testList.Get(1)) == 'D' && *(testList.Get(2)) == 'B')
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin Create a List, add 100 items
		cout << endl << "Test 3" << endl;
		List<char> testList;


		for (int i = 0; i < 100; i++)
		{
			testList.PushBack(1);
		}

		bool expectedInsert = false;
		bool actualInsert = testList.Insert(1, 'D');

		cout << "Expected insert: " << expectedInsert << endl;
		cout << "Actual insert:   " << actualInsert << endl;
		cout << "Expected size: " << "100" << endl;
		cout << "Actual size:   " << testList.Size() << endl;

		if (actualInsert == expectedInsert && testList.Size() == 100)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end


}
