#ifndef _COURSE_NOT_FOUND_EXCEPT_HPP
#define _COURSE_NOT_FOUND_EXCEPT_HPP

#include <stdexcept>
using namespace std;

// If the stack is empty when the Top() function is called, this is the exception that will be thrown.

class CourseNotFound : public runtime_error
{
public:
	CourseNotFound(const string& error)
		: runtime_error(error)
	{
		 // Nothing is needed in here, it's just calling the parent constructor.
	}
};


#endif
