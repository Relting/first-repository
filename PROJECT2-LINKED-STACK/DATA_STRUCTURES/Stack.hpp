#ifndef _STACK_HPP
#define _STACK_HPP
#include <stdexcept> // Is this required?
#include <iostream>
using namespace std; 
#include "../EXCEPTIONS/CourseNotFoundException.hpp"
#include "Node.hpp"
 
template <typename T>
class LinkedStack // : public LinkedList for inheritance
{
    public: 
    LinkedStack() noexcept // Initialize data for the stack itself, not its nodes
    {
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;
		m_itemCount = 0;
    }

    void Push( const T& newData ) noexcept
    {
		//LinkedList<T>::PushBack(newData); for inheritance

		// Allocating memory
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;

		// Place the node in the list:
		// List is empty
		if (m_ptrLast == nullptr)
		{
			// Point the first & last ptrs to the new node
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}

		// List has at least one item
		else
		{
			// Point the last node's ptrNext to the new node
			// m_ prefix indicates it's the stack's pointer, not the nodes'
			m_ptrLast->ptrNext = newNode;
			newNode->ptrPrev = m_ptrLast;
			m_ptrLast = newNode;
		}

		// Increment item count of the stack:
		m_itemCount++;
    }

    T& Top()
    {
		// if the list is empty
		if (m_ptrFirst == nullptr)
		{
			throw CourseNotFound("Tried to return the top, but the top was empty.");
		}

		return m_ptrLast->data; // No need to dereference? No, the arrow dereferences m_ptrLast
		
    }

    void Pop() noexcept 
    {
		if (m_ptrLast == nullptr)
		{
			cout << "The list was empty, there was nothing to pop." << endl;
			return; // Makes sure you don't go forward through the function and deincrement itemcount
		}

		// If there is only one item in the list, then you need to reassign first/last pointers to null.
		else if (m_ptrFirst == m_ptrLast)
		{
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}

		else
		{
			Node<T>* ptrSecondToLast = m_ptrLast->ptrPrev;

			// Deallocate memory from the node you're popping
			delete m_ptrLast;

			// Now set the list's ptrLast to the new last node
			m_ptrLast = ptrSecondToLast;

			// Now reassign the last node's ptrNext
			m_ptrLast->ptrNext = nullptr;
		}
		// We are removing an item, so deincrement the list's itemcount
		m_itemCount--;
    }

    int Size() noexcept
    {
        return m_itemCount;
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
