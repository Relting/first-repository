#ifndef _function4
#define _function4

#include <string>
using namespace std;

/*
CountConsonants functions
@param string text      The text to count the consonants of
@param int pos          (For the recursive version) current position being investigated

Iterate through each char in the string [text] and count up 1 if that letter is a consonant.
*/

bool IsConsonant( char letter )
{
    if (    tolower( letter ) == 'a' ||
            tolower( letter ) == 'e' ||
            tolower( letter ) == 'i' ||
            tolower( letter ) == 'o' ||
            tolower( letter ) == 'u'
        )
    {
        return false;
    }

    return true;
}

int CountConsonants_Iter( string text )
{
	int consonants = 0;
	for (int i = 0; i < text.size(); i++)
	{
		if (IsConsonant(text[i]))
			consonants++;
	}
    return consonants; // placeholder
}

int CountConsonants_Rec( string text, int pos )
{
   
	// Terminating case
	if (pos == text.size()) // position is at the end
		return 0;
	
	// Recursive case(s)

	if (IsConsonant(text[pos]))
		return 1 + CountConsonants_Rec(text, pos + 1); // If it's a consonant, return one plus whatever the next position/s will return.
	else
		return CountConsonants_Rec(text, pos + 1); // If not, return whatever the next position/s will return.
}

#endif
