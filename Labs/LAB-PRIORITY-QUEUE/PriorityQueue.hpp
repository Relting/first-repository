#ifndef _PRIORITY_QUEUE
#define _PRIORITY_QUEUE

#include <iostream>
#include <iomanip>

using namespace std;

#include "Menu.hpp"
#include "Job.hpp"

const int MAX_SIZE = 1000;

class PriorityQueue
{
    public:
    PriorityQueue();
    void Push( Job* task );
    void Pop();
    Job* Front();
    int Size();
    void Display();

    private:
    void ShiftRight( int atIndex );
    void ShiftLeft( int atIndex );

    Job* m_arr[MAX_SIZE];
    int m_itemCount;
};

PriorityQueue::PriorityQueue()
{
    m_itemCount = 0;
}

/*
	0	1	2	3	4
	8	0	2	4	6
*/

void PriorityQueue::Push( Job* task )
{
	int atIndex = 0;
	if (m_itemCount == MAX_SIZE)
	{
		throw runtime_error("Tried adding an item to a full array!");
	}

	for (atIndex = 0; atIndex < m_itemCount; atIndex++)
	{
		if (task->dueOnDay < m_arr[atIndex]->dueOnDay)
		{
			break;
		}
	}

	ShiftRight(atIndex);
	m_arr[atIndex] = task;
	m_itemCount++;

}

void PriorityQueue::Pop()
{
    ShiftLeft(0);
	m_itemCount--;
}

Job* PriorityQueue::Front()
{
	return m_arr[0];
}

void PriorityQueue::ShiftRight( int atIndex )
{
    for ( int i = m_itemCount; i > atIndex; i-- )
    {
        m_arr[i] = m_arr[i-1];
    }
}

void PriorityQueue::ShiftLeft( int atIndex )
{
    for ( int i = atIndex + 1; i < m_itemCount; i++ )
    {
        m_arr[i-1] = m_arr[i];
    }
}

int PriorityQueue::Size()
{
    return m_itemCount;
}

void PriorityQueue::Display()
{
    cout << endl;
    Menu::Header( "Priority Queue" );
    cout << left
        << setw( 10 ) << "INDEX"
        << setw( 10 ) << "DUE"
        << setw( 50 ) << "ASSIGNMENT" << endl;

    for ( int i = 0; i < m_itemCount; i++ )
    {
        cout << left
            << setw( 10 ) << i
            << setw( 10 ) << m_arr[i]->dueOnDay
            << setw( 50 ) << m_arr[i]->assignment << endl;
    }
}

#endif
