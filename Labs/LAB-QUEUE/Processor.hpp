#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come, First Served" << endl;
	int cycles = 0; // Keeps track of time
	while (jobQueue.Size() > 0)
	{
		output << "Current cycle: " << cycles << endl;
		output << "Currently processing Job # " << jobQueue.Front()->id << "." << endl;
		output << "Time remaining: " << jobQueue.Front()->fcfs_timeRemaining << endl;
		jobQueue.Front()->Work(FCFS);

		if (jobQueue.Front()->fcfs_done)
		{
			//jobQueue.Front()->fcfs_finishTime = cycles; // confirmed way
			jobQueue.Front()->SetFinishTime(cycles, FCFS);  // try it this way

			jobQueue.Pop();
		}

		cycles++;
	}


	for (int i = 0; i < allJobs.size(); i++) 
	{
		cout << "Job ID: " << allJobs[i].id << endl;
		cout << "Time to complete: " << allJobs[i].fcfs_finishTime << endl;
	}

	cout << "The total processing time to complete all jobs is " << allJobs[allJobs.size() - 1].fcfs_finishTime << "." << endl;
	cout << "The average time to complete all jobs is " << allJobs[allJobs.size() - 1].fcfs_finishTime / allJobs.size() << "." << endl;

	output.close();
}

void Processor::RoundRobin(vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile)
{
	ofstream output(logFile);
	output << "RR" << endl;
	int cycles = 0, timer = 0; // Keeps track of time
	while (jobQueue.Size() > 0)
	{

		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
			timer = 0;
		}

		output << "Current cycle: " << cycles << endl;
		output << "Currently processing Job # " << jobQueue.Front()->id << "." << endl;
		output << "Time remaining: " << jobQueue.Front()->rr_timeRemaining << endl;
		output << "Times interrupted: " << jobQueue.Front()->rr_timesInterrupted << endl;
		jobQueue.Front()->Work(RR);

		if (jobQueue.Front()->rr_done)
		{
			//jobQueue.Front()->rr_finishTime = cycles; // confirmed way
			jobQueue.Front()->SetFinishTime(cycles, RR);  // try it this way
			jobQueue.Pop();
		}

		cycles++;
		timer++;
	}


	for (int i = 0; i < allJobs.size(); i++)
	{
		cout << "Job ID: " << allJobs[i].id << endl;
		cout << "Time to complete: " << allJobs[i].fcfs_finishTime << endl;
		cout << "Times interrupted: " << allJobs[i].rr_timesInterrupted << endl;
	}

	cout << "The total processing time to complete all jobs is " << allJobs[allJobs.size() - 1].fcfs_finishTime << "." << endl;
	cout << "The average time to complete all jobs is " << allJobs[allJobs.size() - 1].fcfs_finishTime / allJobs.size() << "." << endl;
	cout << "The round robin interval is " << timePerProcess << "." << endl;
	output.close();
}


#endif
