// Lab - Standard Template Library - Part 2 - Lists
// FIRSTNAME, LASTNAME

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list < string >& states);

int main()
{

	list<string> statesList;

	bool done = false;

	while (!done)
	{
		cout << "Display list: " << endl;
		DisplayList(statesList);
		cout << endl << "MAIN MENU" << endl;
		cout << "1. Add a new state to the front of the list \n"
			<< "2. Add a new state to the back of the list \n"
			<< "3. Pop state from the front of the list \n"
			<< "4. Pop state from the end of the list \n"
			<< "5. Continue \n" << endl;

		int choice;
		cin >> choice;

		if (choice == 1) {	
			string newState;
			cout << "What is the state name? ";
			cin >> newState;
			statesList.push_front(newState);
		}
		else if (choice == 2) {	
			string newState;
			cout << "What is the state name? ";
			cin >> newState;
			statesList.push_back(newState);
		}
		else if (choice == 3) {
			statesList.pop_front();
		}
		else if (choice == 4) {
			statesList.pop_back();
		}
		else if (choice == 5) {
			done = true;
		}
	}

	cout << "\n\n NORMAL LIST: " << endl;
	DisplayList(statesList);

	cout << "\n\n REVERSE LIST: " << endl;
	statesList.reverse();
	DisplayList(statesList);

	cout << "\n\n SORT LIST: " << endl;
	statesList.sort();
	DisplayList(statesList);

	cout << "\n\n SORT-REVERSE LIST: " << endl;
	statesList.reverse();
	DisplayList(statesList);

    cin.ignore();
    cin.get();
    return 0;
}

void DisplayList(list < string >& states)
{
	for (list < string >::iterator it = states.begin();
		it != states.end();
		it++)
	{
		cout << *it << " \t ";
	}
	cout << endl;
}